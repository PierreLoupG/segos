# -- SegOS Makefile ------------------------------------------------------------
# 2022  Pierre-Loup GOSSE

MAJOR		:= 0
MINOR		:= 1
PATCH		:= 0
VERSION	:= $(MAJOR).$(MINOR).$(PATCH)

ARCH		?= x86_64

MAKEFILES := $(shell find . -mindepth 2 -type f -name "Makefile")

SOURCES	:= $(shell find . -type f -regex '.*\.\(c\|S\)' -not -path '*/generated/*')
OBJECTS	:= $(filter %.o,$(SOURCES:.S=.o) $(SOURCES:.c=.o))
include $(MAKEFILES)

DMACRO	:= -DKERNEL_VERSION=\"$(VERSION)\"

INCLUDE	:= $(shell find include -type d -exec echo "-I{}" \;)

CC 			:= gcc
CCFLAGS	:= -nostdlib -nostdinc -fno-builtin -fno-stack-protector \
	-nostartfiles -nodefaultlibs -Wall -Wextra $(INCLUDE) $(DMACRO)

LD			:= ld
LDFLAGS	:= -T boot/kernel.ld -nostdlib -nostdinc -nostartfiles -nodefaultlibs

AS			:= gcc
ASFLAGS	:= -Wall -Wextra -O2 -nostdlib -nodefaultlibs $(INCLUDE)


# -- Recipes -------------------------------------------------------------------

all: kernel.elf

kernel: kernel.elf
kernel.elf: $(OBJECTS)
	$(LD) $(LDFLAGS) $(OBJECTS) -o $@

iso: os.iso
os.iso: kernel.elf grub.cfg
	mkdir -p iso/boot/grub
	cp grub.cfg iso/boot/grub
	cp $< iso/boot/kernel.elf
	grub-mkrescue -o os.iso iso --modules=all_video
	rm -rf iso

run: os.iso
	qemu-system-x86_64 -M kernel-irqchip=off -m 1G --enable-kvm -smp 1 -cdrom $< -monitor stdio

%.o: %.c
	$(CC) $(CCFLAGS) -c $< -o $@

%.o: %.S
	$(AS) $(ASFLAGS) -c $< -o $@

clean:
	rm -rf kernel.elf os.iso $(OBJECTS)

/*
 *  segos/arch/x86_64/boot.S
 *
 *  Copyright (C) 2022  Pierre-Loup GOSSE
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  Enable 64-bits paging, setup GDT and page table, switch to long mode and
 *  jump to the kernel "kstart" (segos/init/init.c) function.
 */

#include <sys/multiboot2.h>
#include <sys/paging.h>
#include <sys/gdt.h>

	.section ".multiboot2"
	.align 64

/*
 * Multiboot2 header
 * We use the Multiboot2 specification to boot our ELF x86 64 bit kernel.
 * The header must follow the layout:
 * - magic number   (u32)
 * - architecture   (u32)
 * - header_length  (u32)
 * - checksum       (u32)
 * - tags
 *
 * See https://www.gnu.org/software/grub/manual/multiboot2/multiboot.html.
 */
.header_start:
	.long   MULTIBOOT2_HEADER_MAGIC
	.long   0
	.long   (.header_end - .header_start)
	.long   -(MULTIBOOT2_HEADER_MAGIC + 0 + (.header_end - .header_start))

	/* Entry tag */
	.balign 8
	.word   MULTIBOOT_HEADER_TAG_ENTRY_ADDRESS
	.word   0
	.long   12
	.long   entry

	/* End tag */
	.balign 8
	.word   MULTIBOOT_HEADER_TAG_END
	.word   0
	.long   8
.header_end:

/*
 * Entry of our kernel, setup the long mode and jump to kernel start function.
 * We are currently running in protective mode (32-bits). The CPU use the
 * segmentation memory management with initialized 32-bits GDT from GRUB
 * Multiboot2.
 * The purpose of the following instructions is to setting up the long mode
 * for executing our 64-bits kernel. To do so, we need to:
 * - Setup a page table, since segmentation memory management is not used
 *   anymore for 64-bits.
 * - Enable paging and use the page table for virtual-physical address
 *   translation.
 * - Setup some CPU Register flags.
 * - Setup a 64-bits GDT and load it.
 * - Jump to long mode and call the kernel start function.
 *
 * See https://wiki.osdev.org/Setting_Up_Long_Mode to read more about the steps.
 */
	.section ".text"
	.balign 8
	.code32                             /* Use the x86 32-bits instructions set */
entry:
	/*
	 * Load the CR3 with the Page Map Level 4 (PML4) of the page table.
	 * Once paging enable, the CR3 will be used for the virtual-physical
	 * address translation.
	 *
	 * Note: since CPU Register is a special register we can only load value from
	 * another register. "pml4" being a label, we must first load it to eax.
	 *
	 * See https://wiki.osdev.org/Paging.
	 */
	movl    $pml4, %eax
	movl    %eax, %cr3

	/*
	 * Set the Physical Address Extension (PAE) flag (bit 5) in the CR4.
	 * Since the CPU starts in protected mode (32-bits) the address space is
	 * limited to 4 GB, the CPU can only load 32 bits addresses. But, our page
	 * table, loaded in the CR3, have 64 bits entries. During address translation
	 * the CPU would fail to access 64 bits addresses.
	 * The PAE is a memory management feature for x86 allowing a running 32-bits
	 * CPU to access 64 bits addresses.
	 * Once in long mode, the CR4 is no longer used and the PAE is useless since
	 * the CPU is natively running in 64-bits.
	 *
	 * See https://en.wikipedia.org/wiki/Control_register#CR4 and
	 * https://en.wikipedia.org/wiki/Physical_Address_Extension.
	 */
	movl    %cr4, %eax                                    /* Load the CR4 value */
	orl     $0x20, %eax                                     /* Set the PAE flag */
	movl    %eax, %cr4                            /* Write the edited CR4 value */

	/*
	 * Set the Long Mode Enable (LME) flag (bit 8) in Extended Feature Enable
	 * Register (EFER). The EFER is a Model-Specific Register (MSR) for x86 CPU.
	 * MSR can be accessed with "rdmsr" and "wrmsr" instructions.
	 * To set the LME flag we must first read the MSR EFER with "rdmsr"
	 * instruction by placing his address to the ecx register, set the 8 bit to
	 * 1 and write the MSR with "wrmsr".
	 *
	 * See https://wiki.osdev.org/CPU_Registers_x86-64#IA32_EFER and
	 * https://wiki.osdev.org/MSR to read more about the MSR/EFER.
	 */
	movl    $0xc0000080, %ecx        /* Load the address of the MSR EFER to ecx */
	rdmsr                                           /* Read the MSR EFER to eax */
	orl     $0x100, %eax                                    /* Set the LME flag */
	wrmsr                                 /* Write the edited MSR EFER from eax */

	/*
	 * Set the Paging (PG) and Write Protect (WP) flags in the CR0.
	 * The CR0 is a 32 bits long register with control flags to modify the basic
	 * operation of the processor.
	 * By setting the PG flag (bit 31) to 1 we enable the paging and the CPU will
	 * use the CR3 register for the virtual-physical address translation.
	 * By setting the WP flag (bit 16) to 1 the CPU cannot write to read-only
	 * pages when privilege level is 0 (i.e. kernel page).
	 *
	 * See https://en.wikipedia.org/wiki/Control_register#CR0.
	 */
	movl    %cr0, %eax                                    /* Load the CR0 value */
	orl     $0x80000100, %eax                          /* Set the PG & WP flags */
	movl    %eax, %cr0                            /* Write the edited CR0 value */

	/*
	 * Load the 64-bits GDT structure from the GDTR and jump to the long mode.
	 * The system is setup to run in long mode (64-bits), but we are still
	 * currently using the 32-bits GDT initialized by GRUB Multiboot2 at startup.
	 * We need to load our defined 64-bits GDT structure with the "lgdt"
	 * instruction and use the "ljump" instruction to jump to the "boot" label.
	 * The "ljump" allow to jump to an address from another Code Segment (CS).
	 * Indeed, we are running with the 32-bits GDT CS but the "boot" are located
	 * in the 64-bits GDT CS.
	 * The "ljump" take two arguments:
	 * - segment selector: offset of the segment descriptor to load into the CS.
	 * - address: address to load in the Extended Instruction Pointer (EIP).
	 * Our "boot" label is in the kernel code segment, the second entry in our
	 * GDT, so the segment selector is "0x8" (GDT entry is 8 bytes long).
	 *
	 * See https://www.felixcloutier.com/x86/lgdt:lidt to read more about the
	 * "lgdt" instruction.
	 * See https://wiki.osdev.org/Global_Descriptor_Table#Table for the offset.
	 * See https://docs.oracle.com/cd/E19620-01/805-4693/instructionset-73/index.html
	 * to read more about the "ljmp" instruction.
	 */
	lgdt    gdtr64                        /* Load the GDT from the GDT Register */
	ljmp    $0x8, $boot                  /* Load the Kernel CS and jump to boot */

	.code64                             /* Use the x86 64-bits instructions set */
boot:
	/*
	 * From this point we are executing in long mode (64-bits).
	 * We can now load the 64-bits kernel data segment, third entry of our GDT
	 * (offset 0x10), to the ax register and load it to the DS, ES, FS, GS and SS
	 * segment registers.
	 * Even if the GDT is not really used in 64-bits since the CPU use the page
	 * table for address translation, is encouraged to load the segment registers.
	 *
	 * See https://wiki.osdev.org/CPU_Registers_x86-64#Segment_Registers.
	 */
	movw    $0x10, %ax
	movw    %ax, %ds
	movw    %ax, %es
	movw    %ax, %fs
	movw    %ax, %gs
	movw    %ax, %ss

	/*
	 * Setup an initial stack of KiB and call the kstart function from C code.
	 * The "kstart" label is from segos/init/init.c.
	 * GRUB Multiboot2 pushed the Multiboot information structure address (mb2) in
	 * the ebx register. We want to give this address as the first argument of our
	 * kstart function. It is the purpose of the rdi register.
	 * Given that Multiboot2 setup the system into protected mode, the ebx
	 * register is a 32-bits register. So we cannot move the ebx value to rdi.
	 * But we can simply move the ebx value to edi which is the 32-bits version of
	 * the rdi register.
	 *
	 * See http://6.s081.scripts.mit.edu/sp18/x86-64-architecture-guide.html to
	 * read more about x86_64 registers.
	 */
	movq    $boot_stack, %rsp                          /* Set the stack pointer */
	movq    $kstart, %rax                     /* Load the kstart address to rax */
	xorq    %rdi, %rdi                                /* Clear the rdi register */
	movl    %ebx, %edi             /* Set the mb2 address as the first argument */
	jmpq    *%rax                                       /* Jump to the kernel ! */

/*
 * Setup the 64-bits Global Descriptor Table (GDT)
 * The GDT is a binary data structure containing entries (segment descriptor)
 * telling the CPU about memory segments. Although we use the paging (Page Table)
 * mechanism rather than segementation, we need to initialize a GDT for the
 * CPU with dummy entries. Indeed, for some operation the CPU need to load
 * segment descriptor into segment register. For exemple, during interruption
 * the CPU need to load the kernel code segment descriptor into the Code Segment
 * (CS) register before calling the Interrupt Service Routine (ISR).
 * See https://wiki.osdev.org/Segmentation to read more about segmentation.
 *
 * A GDT entry (Segment Descriptor) is composed of 4 fields:
 * - base: linear address where the segment begins
 * - limit: maximum addressable unit from the base address
 * - access byte: bit mask
 * - flags: bit mask
 *
 * See segos/include/sys/gdt.h.

 * Since we are in 64-bits mode with the paging mechanism to access addresses,
 * the base and limit fields are useless and set to 0. Only access byte and
 * flags fields need to be set.
 * See https://wiki.osdev.org/Global_Descriptor_Table to read more about GDT
 * and https://wiki.osdev.org/GDT_Tutorial to setup a GDT.
 *
 * The GDT register (GDTR) need to be initialized pointing to the GDT data
 * structure. The GDT is load from the GDTR with the "lgdt" instruction.
 */
	.section ".rodata"
	.globl tss64
	.globl gdtr64
/* GDT data structure */
gdt64:
	/* Null descriptor (offset 0x0) */
	.quad   0
	/* Kernel mode code segment (offset 0x8) */
	.word 0                                                     /* Limit (0-15) */
	.word 0                                                     /* Base (16-31) */
	.byte 0                                                     /* Base (32-39) */
	.byte (SD_P | SD_DPL_K | SD_NOT_SYS | SD_E | SD_RW)  /* Access Byte (40-47) */
	.byte (SD_L) << 4                          /* Limit (48-51) & Flags (52-55) */
	.byte 0                                                     /* Base (56-63) */
	/* Kernel mode data segment (offset 0x10) */
	.word 0                                                     /* Limit (0-15) */
	.word 0                                                     /* Base (16-31) */
	.byte 0                                                     /* Base (32-39) */
	.byte (SD_P | SD_DPL_K | SD_NOT_SYS | SD_RW)         /* Access Byte (40-47) */
	.byte 0                                    /* Limit (48-51) & Flags (52-55) */
	.byte 0                                                     /* Base (56-63) */
	/* Task Stack Segment (offset 0x18) see segos/include/sys/tss.h */
tss64:
	.word 0                                                     /* Limit (0-15) */
	.word 0                                                     /* Base (16-31) */
	.byte 0                                                     /* Base (32-39) */
	.byte (SD_P | SD_TYPE_TSS_A)                         /* Access Byte (40-47) */
	.byte 0                                    /* Limit (48-51) & Flags (52-55) */
	.byte 0                                                     /* Base (56-63) */
	.int 0                                                      /* Base (64-95) */
	.int 0                                                 /* Reserved (96-127) */
/* GDT register */
gdtr64:
	.word   gdtr64 - gdt64 - 1                                         /* Limit */
	.long   gdt64                                                       /* Base */

/*
 * Setup the kernel 4-levels Page Table
 * The kernel is located in low addresses (the first 2 MiB) and the addresses
 * are linear (virtual = physical).
 *
 * See segos/include/sys/paging.h.
 */
	.align  0x1000
pml4:
	.quad   pml3 + (PE_U | PE_W | PE_P)
	.space  (PAGE_SIZE - 0x8), 0
pml3:
	.quad   pml2 + (PE_U | PE_W | PE_P)
	.space  (PAGE_SIZE - 0x8), 0
pml2:
	.quad   0x0  + (PE_G | PE_PS | PE_PCD | PE_PWT | PE_W | PE_P)
	.quad   pml1 + (PE_PCD | PE_PWT | PE_W | PE_P)
	.space  (PAGE_SIZE - 0x10), 0
pml1:
	.quad   0xfee00000 + (PE_G | PE_PCD | PE_PWT | PE_W | PE_P)
	.space  (PAGE_SIZE - 0x8), 0

/* Setup stack */
	.section ".bss"
	.space  0x1000, 0                                 /* Initial stack of 4 KiB */
boot_stack:
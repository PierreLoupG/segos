/*
 *  segos/include/sys/apic.h
 *
 *  Copyright (C) 2023  Pierre-Loup GOSSE
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  The Advanced Programmable Interrupt Controller (APIC) is an interrupts
 *  manager. The APIC is composed registers mapped in a 4K-bytes space from
 *  the physical base address 0xfee00000. We mapped the virtual address 0x200000
 *  to the APIC registers, firts entry of PML1 in segos/boot/boot.S.
 *  The APIC registers allow to program the APIC. To simplify the registers
 *  manipulation, we load an "apic" struct from the virtual address.
 *
 *  See https://wiki.osdev.org/APIC
 *  See https://www.amd.com/system/files/TechDocs/24593.pdf#G22.998446
 */

#ifndef _APIC_H_
#define _APIC_H_

#include <types.h>


#define APIC_SPURIOUS_ASE           (1u << 8)

/**
 * apic_register - struct representing an Local APIC register
 *
 * @value: the 4-bytes register space
 * @_pad: padding to be 16-bytes aligned
 *
 * @note since value field is the register value we must take the value from the
 * register every time without caching it. The 'volatile' tell the compiler not
 * to cache the value of this field (https://stackoverflow.com/a/246144/9390440).
 *
 * @see https://www.amd.com/system/files/TechDocs/24593.pdf#G22.999204.
 */
struct apic_register
{
  volatile uint32_t value;
  uint32_t _pad[3];
} __attribute__((packed));

/**
 * apic - struct representing the local APIC registers from memory
 *
 * @see https://www.amd.com/system/files/TechDocs/24593.pdf#G22.999204.
 */
struct apic
{
  struct apic_register _pad0[2];
  struct apic_register id;
  struct apic_register version;
  struct apic_register _pad1[4];
  struct apic_register tpr;
  struct apic_register apr;
  struct apic_register ppr;
  struct apic_register eoi;
  struct apic_register rrr;
  struct apic_register ldr;
  struct apic_register dfr;
  struct apic_register svr;
  struct apic_register isr[8];
  struct apic_register tmr[8];
  struct apic_register irr[8];
  struct apic_register esr;
  struct apic_register _pad2[7];
  struct apic_register icr_low;
  struct apic_register icr_high;
  struct apic_register timer_entry;
  struct apic_register thermal_entry;
  struct apic_register performance_entry;
  struct apic_register local0_entry;
  struct apic_register local1_entry;
  struct apic_register error_entry;
  struct apic_register timer_initial;
  struct apic_register timer_current;
  struct apic_register _pad3[4];
  struct apic_register timer_divide;
  struct apic_register _pad4;
  struct apic_register extended_feature;
  struct apic_register extended_control;
  struct apic_register seoi;
  struct apic_register _pad5[5];
  struct apic_register ier[8];
  struct apic_register extended_vector[3];
} __attribute__((packed));

extern struct apic *apic;

static inline void apic_eoi(void)
{
  apic->eoi.value = 0;
}

static inline void apic_software_enable(void)
{
  apic->svr.value |= APIC_SPURIOUS_ASE;
}

static inline void apic_software_disable(void)
{
  apic->svr.value &= ~APIC_SPURIOUS_ASE;
}

void pic_setup(void);

void pic_disable(void);

void apic_setup(void);

#endif
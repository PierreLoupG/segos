/*
 *  segos/include/sys/gdt.h
 *
 *  Copyright (C) 2022  Pierre-Loup GOSSE
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  The Global Descriptor Table (GDT) is a binary structure. It contains entries
 *  telling CPU about memory segment. The GDT is setup in segos/boot/boot.S.
 *
 *  Glossary:
 *  - Segment: chunck of memory
 *  - Segment Register: a register CPU that refers to a segment
 *    see https://wiki.osdev.org/CPU_Registers_x86-64#Segment_Registers.
 *  - Segment Descriptor: an entry in the GDT
 *  - Segment Selector: an offset of a descriptor in the GDT
 *
 *  For now, 3 segments is set into the GDT binary structure:
 *  - Kernel code
 *  - Kernel data
 *  - TSS
 *  The respective segments selector are defined in segos/include/asm/x86.h.
 *
 *  The User Segment Descriptor structure:
 *  (https://wiki.osdev.org/Global_Descriptor_Table#Segment_Descriptor)
 *  +------+-------+-------+-------------+------+-------+
 *  | Base | Flags | Limit | Access Byte | Base | Limit |
 *  +------+-------+-------+-------------+------+-------+
 *  |63  56|55   52|51   48|47         40|39  16|15    0|
 *
 *  Access Byte:
 *  +---+-----+---+---+----+----+---+
 *  | P | DPL | S | E | DC | RW | A |
 *  +---+-----+---+---+----+----+---+
 *  | 7 |6   5| 4 | 3 | 2  | 1  | 0 |
 *
 *  Flags:
 *  +---+----+---+----------+
 *  | G | DB | L | Reserved |
 *  +---+----+---+----------+
 *  | 3 | 2  | 1 |    0     |
 *
 *  The System Segment Descriptor structure:
 *  (https://wiki.osdev.org/Global_Descriptor_Table#Long_Mode_System_Segment_Descriptor)
 *  +----------+------+------+-------+-------+-------------+------+-------+
 *  | Reserved | Base | Base | Flags | Limit | Access Byte | Base | Limit |
 *  +----------+------+------+-------+-------+-------------+------+-------+
 *  |127     96|95  64|63  56|55   52|51   48|47         40|39  16|15    0|
 *
 *  Access Byte:
 *  +---+-----+---+------+
 *  | P | DPL | S | Type |
 *  +---+-----+---+------+
 *  | 7 |6   5| 4 |3    0|
 *
 *  Flags:
 *  +---+----+---+----------+
 *  | G | DB | L | Reserved |
 *  +---+----+---+----------+
 *  | 3 | 2  | 1 |    0     |
 */

#ifndef _GDT_H_
#define _GDT_H_

/*
 * Segment Descriptor (SD) bits
 */

/* Access Byte */
#define SD_P          1 << 7                                   /* Present Bit */
#define SD_DPL_K      0 << 5             /* Kernel Descriptor Privilege Level */
#define SD_DPL_U      3 << 5               /* User Descriptor Privilege Level */
#define SD_NOT_SYS    1 << 4                          /* Code or data segment */
#define SD_E          1 << 3                                    /* Executable */
#define SD_DC         1 << 2                      /* Direction/Conforming bit */
#define SD_RW         1 << 1                                    /* Read/Write */
#define SD_A          1 << 0                                       /* Acessed */
#define SD_TYPE_LDT   0x2                           /* Local Descriptor Table */
#define SD_TYPE_TSS_A 0x9                                    /* Available TSS */
#define SD_TYPE_TSS_B 0xB                                         /* Busy TSS */
/* Flags */
#define SD_G        1 << 3                                     /* Granularity */
#define SD_SIZE_32  1 << 2                           /* 32-bit protected mode */
#define SD_L        1 << 1                                       /* Long mode */

#endif
/*
 *  segos/include/sys/paging.h
 *
 *  Copyright (C) 2022  Pierre-Loup GOSSE
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  Header file for pagin informations.
 *  Entry of a page table (64 bit) :
 *  +----+------+--------------+------+---+----+---+---+-----+-----+---+---+---+
 *  | NX | Free | Page address | Free | G | PS | D | A | PCD | PWT | U | R | P |
 *  +----+------+--------------+------+---+----+---+---+-----+-----+---+---+---+
 *  | 63 |62  52|51          12|11   9| 8 | 7  | 6 | 5 |  4  |  3  | 2 | 1 | 0 |
 *
 *  PML: Page Map Level
 *  PDE: Page Directory Entry
 *  PTE: Page Table Entry
 *
 * The SegOS Page Table layout:
 * Since the kernel is located in low address (the first 2 MiB: 0x0 -> 0x200000)
 * we map a huge page (2 MiB) at 0x0 virtual address to the 0x0 physical address
 * creating a linear address (virtual = physical).
 *
 *   CR3
 *    @------> PML 4 (PDE)
 *            +-----+
 *            |  @--|--> PML 3 (PDE)
 *            +-----+   +-----+
 *            |  0  |   |  @--|--> PML 2 (PDE)
 *            +-----+   +-----+   +-----+            Huge page (2 MiB)
 *            | ... |   |  0  |   |  @--|--------------> +-----+
 *            +-----+   +-----+   +-----+                |     |
 *            |  0  |   | ... |   |  @--|--> PML 1 (PTE) |     |
 *            +-----+   +-----+   +-----+   +-----+      |     |
 *           0x104000   |  0  |   | ... |   |  0  |      |     |
 *                      +-----+   +-----+   +-----+      |     |
 *                     0x105000   |  0  |   | ... |      |     |
 *                                +-----+   +-----+      +-----+
 *                               0x106000   |  0  |        0x0
 *                                          +-----+
 *                                         0x107000
 *
 *  More informations about paging at https://wiki.osdev.org/Paging.
 */

#ifndef _PAGING_H_
#define _PAGING_H_

#define PAGE_SIZE 0x1000                                 /* 4KiB (small page) */

/* Page Entry (PE) bits */
#define PE_P      1 << 0                             /* Present (valid entry) */
#define PE_W      1 << 1                                        /* Read/Write */
#define PE_U      1 << 2                                   /* User/Supervisor */
#define PE_PWT    1 << 3                                     /* Write-Through */
#define PE_PCD    1 << 4                                     /* Cache disable */
#define PE_A      1 << 5                                          /* Accessed */
#define PE_D      1 << 6                                             /* Dirty */
#define PE_PS     1 << 7                                         /* Page Size */
#define PE_G      1 << 8                                            /* Global */
#define PE_NX     1 << 63                                   /* Not Executable */

#endif

/*
 *  segos/include/sys/tss.h
 *
 *  Copyright (C) 2023  Pierre-Loup GOSSE
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  Inherent from the protected mode, the TSS structure is used by the hardware
 *  multi-tasking to saved current task informations. Since hardware multi-
 *  tasking is not supported in long mode, the TSS is not used for multi-tasking
 *  but holds informations: stack pointers for privilege levels,
 *  interrupt-stack-table (IST) pointers and I/O permissions.
 *
 *  The TSS is System Segment Descriptor, and entry of the GDT, 128 bits long.
 *  See tss64 label in segos/boot.S file.
 */

#ifndef _TSS_H_
#define _TSS_H_

#include <types.h>


/* Macro helpers for loading TSS into its GDT entry */
#define TSS_ADDR_LOW(addr)			\
	( ((((addr) >>  0) & 0x0000ffff) << 16)	\
	| ((((addr) >> 16) & 0x000000ff) << 32)	\
	| ((((addr) >> 16) & 0x0000ff00) << 48))

#define TSS_ADDR_HIGH(addr)			\
	( ((((addr) >> 32) & 0xffffffff) <<  0))

#define TSS_SIZE(size)					\
	( (((((size) - 1) >>  0) & 0xffff) <<  0)	\
	| (((((size) - 1) >> 16) & 0x000f) << 48))

/**
 * task_state_segment - struct for 64-bits TSS
 * @_reserved: field to ignore (padding)
 * @rsp: the Stack Pointer for each privilege level (0, 1, 2)
 * @ist: Interrupt Stack Table pointers for IST mechanism
 * @iopb_off: offset of the I/O permission bit map from the struct base address
 * @iopb: the I/O permission bit map (up to 8Kbytes = 64K I/O ports)
 *
 * See https://wiki.osdev.org/Task_State_Segment#Long_Mode
 * and https://www.amd.com/system/files/TechDocs/24593.pdf#G18.933305
 */
struct task_state_segment
{
	uint32_t _reserved0;
	uint64_t rsp0;
	uint64_t rsp1;
	uint64_t rsp2;
	uint64_t _reserved1;
	uint64_t ist1;
	uint64_t ist2;
	uint64_t ist3;
	uint64_t ist4;
	uint64_t ist5;
	uint64_t ist6;
	uint64_t ist7;
	uint64_t _reserved2;
	uint16_t _reserved3;
	uint16_t iopb_off;
	uint64_t iopb;
} __attribute__((packed));

extern struct task_state_segment tss;

void tss_setup(void);

#endif
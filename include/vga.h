#ifndef _VGA_H_
#define _VGA_H_


#include <types.h>


void clear(void);

void putc(char c);

void puts(const char *str, size_t n);


#endif

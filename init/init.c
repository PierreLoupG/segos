/*
 *  segos/init/init.c
 *
 *  Copyright (C) 2022  Pierre-Loup GOSSE
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  Entry point of the kernel.
 */

#include <sys/multiboot2.h>
#include <sys/idt.h>
#include <sys/tss.h>
#include <asm/x86.h>
#include <sys/apic.h>
#include <printk.h>
#include <vga.h>

__attribute__((noreturn))
void die(void)
{
	hlt();

	while (1);																							 /* Avoid GCC error */
}

__attribute__((noreturn))
void kstart(void *mb2)
{
	clear();
	printk("Welcome to SegOS v%s !\n", KERNEL_VERSION);
	idt_setup();
	tss_setup();

	pic_setup();
	pic_disable();
	apic_setup();

	timer_setup();
	sti();

	die();
}

/*
 *  segos/kernel/apic.c
 *
 *  Copyright (C) 2023  Pierre-Loup GOSSE
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  Even if the legacy PIC is not used we must initialize it. We disable the PIC
 *  in favor of the APIC. To enable the APIC we must set the APIC Enable (AE)
 *  bit of the APIC MSR (0x1b) and set the APIC Software Enable (ASE) bit of the
 *  Spurious interrupt Vectory Register (SVR) to enable the local APIC.
 */

#include <sys/apic.h>
#include <types.h>
#include <asm/x86.h>
#include <sys/idt.h>


#define PIC_MASTER                  0x20
#define PIC_SLAVE                   0xa0
#define PIC_MASTER_COMMAND_PORT     PIC_MASTER
#define PIC_MASTER_DATA_PORT        (PIC_MASTER + 1)
#define PIC_SLAVE_COMMAND_PORT      PIC_SLAVE
#define PIC_SLAVE_DATA_PORT         (PIC_SLAVE + 1)

#define APIC_BASE_MSR               0x1b
#define APIC_BASE_ENABLE            (1ul << 11)
#define APIC_DISABLE                (1u << 16)
#define APIC_VADDR                  0x200000

/*
 * The APIC registers are memory-mapped into a 4-Kbytes space from the physical
 * base address 0xfee00000. In segos/boot/boot.S we directly map the virtual
 * address 0x200000 (first entry of PML1) to the physical address 0xfee00000.
 */
struct apic *apic = (struct apic *) APIC_VADDR;

/**
 * pic_setup - initialisation of the 8259 Programmable Interrupt Controller (PIC)
 *
 * Setup the master and slave PIC chip by sending the initialisation command and
 * remap the PIC with 3 "initialisation words".
 *
 * See https://wiki.osdev.org/8259_PIC#Initialisation.
 */
void pic_setup(void)
{
  uint8_t master_mask, slave_mask;

  /* First, we save the interrupt mask of the PIC by reading the data port */
  master_mask = in8(PIC_MASTER_DATA_PORT);
  slave_mask = in8(PIC_SLAVE_DATA_PORT);

  /* Next, we start the initialisation sequence */
  out8(PIC_MASTER_COMMAND_PORT, 0x11);
  iowait();
  out8(PIC_SLAVE_COMMAND_PORT, 0x11);
  iowait();

  /* Send the first word to set the vector offset */
  out8(PIC_MASTER_DATA_PORT, 0x20);
  iowait();
  out8(PIC_MASTER_DATA_PORT, 0x28);
  iowait();

  /* Send the second word to set the PIC identity */
  out8(PIC_MASTER_DATA_PORT, 0x04);
  iowait();
  out8(PIC_SLAVE_DATA_PORT, 0x02);
  iowait();

  /* Send the third word to set 8086 mode */
  out8(PIC_MASTER_DATA_PORT, 0x01);
  iowait();
  out8(PIC_SLAVE_DATA_PORT, 0x01);
  iowait();

  /* Restore the mask */
  out8(PIC_MASTER_DATA_PORT, master_mask);
  out8(PIC_SLAVE_DATA_PORT, slave_mask);
}

/**
 * pic_disable - disable the PIC
 *
 * If PIC is not used, in favor of APIC, we disable the PIC master and slave
 * chips by sending 0xff to data port.
 */
void pic_disable(void)
{
  out8(PIC_MASTER_DATA_PORT, 0xff);
  out8(PIC_SLAVE_DATA_PORT, 0xff);
}

/**
 * spurious_interrupt - interrupt handler for INT_USER_SPURIOUS
 *
 * Since In-Service Register (ISR) is not affected by spurious interrupt, we do
 * not acknowledge the interrupt by sending an End Of Interrupt (EOI) signal.
 *
 * @see https://www.amd.com/system/files/TechDocs/24593.pdf#G22.1001473
 */
static void spurious_interrupt(struct interrupt_context *ctx
  __attribute__((unused)))
{
  /* Do nothing */
}

/**
 * apic_setup - setup the APIC registers
 *
 * Disable APIC temporally to reset value and enable APIC.
 * We must set the Spurious interrupt Vector Register (SVR) with a vector
 * (interrupt number) and set the APIC Software Enable (ASE) bit to 1 to enable
 * the local APIC (impact only LVT entries).
 *
 * @see https://www.amd.com/system/files/TechDocs/24593.pdf#G22.998864
 * @see https://www.amd.com/system/files/TechDocs/24593.pdf#G22.1001473
 * @see https://xem.github.io/minix86/manual/intel-x86-and-64-manual-vol3/o_fe12b1e2a880e0ce-390.html
 */
void apic_setup(void)
{
  /* Read MSR APIC value */
  uint64_t msr_value = rdmsr(APIC_BASE_MSR);

  /* Disable APIC */
  wrmsr(APIC_BASE_MSR, msr_value & ~APIC_BASE_ENABLE);

  /*
   * Set Task Priority Register (TPR).
   * The TPR works as a threshold to block all interrupts that are less than or
   * equal to the set value.
   */
  apic->tpr.value = 0;

  wrmsr(APIC_BASE_MSR, msr_value | APIC_BASE_ENABLE);

  /* Set the spurious interrupt */
  interrupt_vector[INT_USER_SPURIOUS] = spurious_interrupt;

  /* Software enable the local APIC and set the spurious vector */
  apic->svr.value |= (INT_USER_SPURIOUS | APIC_SPURIOUS_ASE);
}
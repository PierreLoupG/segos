/*
 *  segos/kernel/idt.c
 *
 *  Copyright (C) 2022  Pierre-Loup GOSSE
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  Setup the Interrupt Descriptor Table (IDT).
 */

#include <sys/idt.h>
#include <asm/x86.h>
#include <types.h>
#include <printk.h>

extern vaddr_t trap_vector[];

/**
 * idt_entry_t - struct for IDT entry aka gate descriptor
 * @isr_low: the lower 16 bits of the ISR's address
 * @segement_selector: the code segment selector that the CPU will load
 * @ist: offset into the Interrupt Stack Table (IST) stored in TSS
 * @flags: gate descriptor flags (Gate type, DPL, P)
 * @isr_mid: the higher 16 bits of the lower 32 bits of the ISR' address
 * @isr_high: the higher 32 bits of the ISR's address
 * @_reserved: set to zero
 *
 * See https://wiki.osdev.org/Interrupts_tutorial#Entries and
 * https://wiki.osdev.org/Interrupt_Descriptor_Table#Gate_Descriptor_2.
 *
 * @note even if the layout structure fits so there is no padding between the
 * fields (16+16)+(16+16)+(32+32) is recommended to use the "packed" attribute
 * to avoid internal padding since a gate descriptor must be 16 bytes long.
 */
typedef struct {
	uint16_t isr_low;
	uint16_t segment_selector;
	uint8_t  ist;
	uint8_t  flags;
	uint16_t isr_mid;
	uint32_t isr_high;
	uint32_t _reserved;
} __attribute__((packed)) idt_entry_t;

/* Aligned IDT (256 entries) */
static __attribute__((aligned(0x10))) idt_entry_t idt64[INTERRUPT_VECTOR_SIZE];

/**
 * idtr64 - the IDT Register data structure
 * @limit: size -1 of the IDT
 * @base: address of the IDT
 *
 * @note since the IDTR must be 10 bytes long, we use a packed structure
 * (i.e. no internal padding) such that the memory size of the structure
 * is 16 + 64 = 80 bits = 10 bytes rather than (with padding) 64 + 64 =
 * 128 bits = 16 bytes.
 * See https://wiki.osdev.org/Interrupt_Descriptor_Table#IDTR.
 */
struct {
	uint16_t limit;
	uint64_t base;
} __attribute__((packed)) idtr64;

interrupt_handler_t interrupt_vector[INTERRUPT_VECTOR_SIZE];

/**
 * idt_setup - setup IDT entries
 *
 * Map the IDT entries to their relative trap handler function. See the
 * generated segos/kernel/generated/trap.S assembly file for the trap_vector
 * definition.
 */
void idt_setup(void)
{
	size_t i;

	/* Setup IDTR */
	idtr64.limit = (sizeof(idt64) - 1);
	idtr64.base = (uint64_t) &idt64;

	/* Setup IDT entries */
	for (i = 0; i < INTERRUPT_VECTOR_SIZE; i++) {
		/* Map the relative trap handler function */
		idt64[i].isr_low  = ((trap_vector[i] >>  0) & 0x0000ffff);
		idt64[i].isr_mid  = ((trap_vector[i] >> 16) & 0x0000ffff);
		idt64[i].isr_high = ((trap_vector[i] >> 32) & 0xffffffff);

		/* We want to load the kernel code segment (offset 0x08 of GDT) */
		idt64[i].segment_selector = KERNEL_CODE_SELECTOR;

		/*
		 * Setup IST offset to 0 leads to use the long-mode stack switches
		 * mechanism rather than the IST mechanism.
		 * If the the offset != 0 the CPU load the corresponding stack pointer from
		 * the IST into RSP. Thus, the ist field must be set with a know-good stack
		 * in setup_tss function from segos/kernel/tss.c file.
		 *
		 * See https://www.amd.com/system/files/TechDocs/24593.pdf#G8.922444
		 * and https://www.amd.com/system/files/TechDocs/24593.pdf#G18.933305
		 * and https://www.amd.com/system/files/TechDocs/24593.pdf#G14.908888.
		 */
		idt64[i].ist = 0;

		/*
		 * If interrupt is a syscall, set the gate type flag to trap gate.
		 * Otherwise set the gate type flag to interrupt gate.
		 */
		if (i == INT_USER_SYSCALL) {
			idt64[i].flags = TRAP_GATE_TYPE;
		} else {
			idt64[i].flags = INTERRUPT_GATE_TYPE;
		}
	}

	/*
	 * Load the IDTR data structure. CPU will now use our IDT for handling
	 * interrupts.
	 * See https://www.felixcloutier.com/x86/lgdt:lidt.
	 */
	asm volatile ("lidt idtr64");
}

/**
 * default_handler - default interrupt handler if not defined
 * @param ctx interrupt context
 *
 * For now, we print the undefined interrupt and halt the system.
 */
void default_handler(struct interrupt_context *ctx)
{
	printk("Interrupt handler %lu not defined (error code = %#lx)\n", ctx->itnum,
		ctx->errcode);

	hlt();
}

/**
 * trap - generic function for handling interrupt
 * @param ctx interrupt context
 *
 * The caller function is __trap from the generated assembly file
 * segos/kernel/generated/trap.S. The __trap function put as first argument
 * the rsp value. By casting the address to interrupt_context struct we can
 * access to the stack values such as the interrupt number "itnum" fields
 * pushed by the trap handler.
 */
void trap(struct interrupt_context *ctx)
{
	interrupt_handler_t handler = interrupt_vector[ctx->itnum];

	if (handler == NULL)
		default_handler(ctx);
	else
		handler(ctx);
}
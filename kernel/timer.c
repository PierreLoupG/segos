/*
 *  segos/kernel/timer.c
 *
 *  Copyright (C) 2023  Pierre-Loup GOSSE
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  Setup the APIC Timer.
 *
 *  See https://www.amd.com/system/files/TechDocs/24593.pdf#G22.1000026.
 */

#include <sys/timer.h>
#include <sys/apic.h>
#include <sys/idt.h>

#define TIMER_PERIODIC    (1u << 17)
#define TIMER_DIVIDE      3                /* Divide value by 16 at each tick */
#define TIMER_INITIAL     0x1000

static void timer_interrupt(struct interrupt_context *ctx
  __attribute__((unused)))
{
  apic_eoi();
}

void timer_setup(void)
{
  interrupt_vector[INT_USER_TIMER] = timer_interrupt;
  apic->timer_entry.value = INT_USER_TIMER | TIMER_PERIODIC;
  apic->timer_divide.value = TIMER_DIVIDE;
  apic->timer_initial.value = TIMER_INITIAL;
}
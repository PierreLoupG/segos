/*
 *  segos/kernel/tss.c
 *
 *  Copyright (C) 2023  Pierre-Loup GOSSE
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  Setup the Task State Segment (TSS).
 */

#include <sys/tss.h>
#include <kernel.h>
#include <asm/x86.h>
#include <string.h>


__attribute__((aligned(0x1000))) struct task_state_segment tss;

/* TSS GDT entry from segos/boot/boot.S */
extern uint64_t tss64[2];

/**
 * tss_setup - Setup the TSS segment
 */
void tss_setup(void)
{
  paddr_t tss_addr = (paddr_t) &tss;

  /* Set to zero each fields */
  memset(tss_addr, 0, sizeof(tss));

  /* Stack Pointer (SP) for Privilege Level (PL) 0 */
  tss.rsp0 = store_rsp();

  /* Offset of the IOPB */
  tss.iopb_off = offsetof(struct task_state_segment, iopb);

  /*
   * Since we do not use the Iterrupt Stack Table (IST) has stack switches
   * mechanism, we leave the ist fields empty.
   */

  /*
   * Setup the I/O Permission Bitmap.
   * Each bit in the IOPB corresponds to a byte I/O port. Access is granted to
   * an I/O port if all corresponding bits are set to 0.
   * By setting each bits in the IOPB to 1 we denied access for all I/O port
   * (only when IOPB is checked by the processor).
   *
   * See https://www.amd.com/system/files/TechDocs/24593.pdf#G18.968963.
   */
  tss.iopb = 0xffffffffffffffff;

  /* Load the TSS Descriptor into its GDT entry */
  tss64[0] |= TSS_ADDR_LOW(tss_addr);
  tss64[0] |= TSS_SIZE(sizeof(tss));
  tss64[1] |= TSS_ADDR_HIGH(tss_addr);

  /*
   * Load the TSS Descriptor into the Task Register.
   * The Task Register is composed of a visible part which can be set from the
   * software and a hidden part from the software. By using the LTR instruction
   * below, we load into the hidden part of the Task Register the TSS Descriptor
   * initialized above in the GDT.
   *
   * See https://www.amd.com/system/files/TechDocs/24593.pdf#G18.917066.
   */
  load_tr(TSS_SELECTOR);
}